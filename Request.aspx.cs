﻿
using System;
using System.Web.UI;

/// <summary>
/// The code behind file for Request.apsx that handles the events to be fired.
/// </summary>
/// <author>
/// Destiny Harris
/// CS 4985 Adv. Web Tech
/// M. Orsega
/// </author>
/// <version>
/// January 27, 2015 | Spring
/// </version>
public partial class Request : Page
{

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.txtArrivalDate.Text = DateTime.Today.ToString("d");
        this.rblBedType.SelectedIndex = 0;
        this.txtSprecialRequest.Text = "";
        this.txtFirstName.Text = "";
        this.txtLastName.Text = "";
        this.txtEmail.Text = "";
        this.txtPhoneNumber.Text = "";
    }

    /// <summary>
    /// Handles the Click event of the btnClear control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnClear_Click(object sender, EventArgs e)
    {
       this.Page_Load(sender, e);
    }

    /// <summary>
    /// Handles the Click event of the btnSubmit control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        this.lblMessage.Text = "Thank you for your request." + "<br>" +
                               "We will get back to you within 24 hours.";
    }
}