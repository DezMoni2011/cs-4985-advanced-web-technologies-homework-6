﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Request.aspx.cs" Inherits="Request" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Chapter 6: Happy Earwig</title>
    <link href="Styles/Main.css" rel="stylesheet" />
    <link href="Styles/Request.css" rel="stylesheet" />
</head>
<body>
    <p>
    <asp:Image ID="imgBanner" runat="server" ImageUrl="~/Images/The_Happy_Earwig_Motel.png" />
    </p>
    <header>
        <h1>The Happy Earwig Motel</h1>
        <h2>Our crawl-space now body-free!</h2>
    </header>
    <section>
        <form id="form1" runat="server" DefaultFocus="txtArrivalDate" DefaultButton="btnSubmit">
            <h1>Reservation Request</h1>
            <h2>Request data</h2>
            <label class="label">Arrival</label>
            <asp:TextBox ID="txtArrivalDate" CssClass="entry" runat="server" TextMode="Date" TabIndex="1"></asp:TextBox> 
            <br />
            <label class="label">Departure</label>
            <asp:TextBox ID="txtDepartureDate" CssClass="entry" runat="server" TextMode="Date" TabIndex="2"></asp:TextBox>
            <br />
            <label class="label">Number of adults</label>
            <asp:DropDownList ID="ddlAdultCount" runat="server" Height="16px" Width="41px" TabIndex="3">
                <asp:ListItem>1</asp:ListItem>
                <asp:ListItem>2</asp:ListItem>
                <asp:ListItem>3</asp:ListItem>
                <asp:ListItem>4</asp:ListItem>
            </asp:DropDownList>

            <br />          

            <label class="label">Bed type</label>
            <asp:RadioButtonList ID="rblBedType" runat="server"  RepeatDirection="Horizontal" Height="16px" TabIndex="4" Width="394px">
                <asp:ListItem Selected="True">King</asp:ListItem>
                <asp:ListItem>Two Queens</asp:ListItem>
                <asp:ListItem>One Queen</asp:ListItem>
            </asp:RadioButtonList>

            <br />

            <h2>Special requests</h2>
            <asp:TextBox ID="txtSprecialRequest" runat="server" TextMode="MultiLine" TabIndex="5"></asp:TextBox>
            <br />
       
            <h2>Contact information</h2>
            <label class="label">First name</label>
            <asp:TextBox ID="txtFirstName" CssClass="entry" runat="server" TabIndex="6"></asp:TextBox>
            <br />
            <label class="label">Last name</label>
            <asp:TextBox ID="txtLastName" CssClass="entry" runat="server" TabIndex="7"></asp:TextBox>
            <br />
            <label class="label">Email address</label>
            <asp:TextBox ID="txtEmail" CssClass="entry" runat="server" TextMode="Email" TabIndex="8"></asp:TextBox>
            <br />
            <label class="label">Telephone number</label>
            <asp:TextBox ID="txtPhoneNumber" CssClass="entry" runat="server" TextMode="Phone" TabIndex="9"></asp:TextBox>
            <br />
            <label class="label">Preferred method</label>
            <asp:DropDownList ID="ddlPerfferedMethod" runat="server" Height="16px" Width="90px" TabIndex="10">
                <asp:ListItem Selected="True">Email</asp:ListItem>
                <asp:ListItem>Phone</asp:ListItem>
            </asp:DropDownList>
            <br />

            <label class="label">&nbsp;</label>
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="button" TabIndex="11" OnClick="btnSubmit_Click" />&nbsp;
            <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="button" TabIndex="12" OnClick="btnClear_Click" /><br />
            <p>
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </p>
    </form>
    </section>
    <footer>
        <p>&copy; 2015, Happy Earwig Motel</p>
    </footer>
</body>
</html>
